#pragma once
#include "CGLMaterial.h"
#include "CGLTexture.h"
#include "BoardState.h"
class CGLRenderer
{
public:
	CGLRenderer(void);
	virtual ~CGLRenderer(void);

	bool CreateGLContext(CDC* pDC);			// kreira OpenGL Rendering Context
	void PrepareScene(CDC* pDC);			// inicijalizuje scenu,
	void Reshape(CDC* pDC, int w, int h);	// kod koji treba da se izvrsi svaki put kada se promeni velicina prozora ili pogleda i
	void DrawScene(CDC* pDC);				// iscrtava scenu
	void DestroyScene(CDC* pDC);			// dealocira resurse alocirane u drugim funkcijama ove klase,
	void OnKeyDown(UINT nChar);
	void PrepareLighting();
	void PrepareMaterials();

	// Materijali
	CGLMaterial matTeapot, matCube, matTable, matCrvena, matPlava, matNarndzasta, matZelena, matBela, matZuta;

	// Teksture
	CGLTexture m_tex1, m_tex2, m_tex3, m_black_tex, m_white_tex, m_crep_tex, m_grass_tex, m_tex_camomreal, m_stone_wall_tex, m_dirt_tex, m_opengl_tex;

	// polozaj oka i centra
	double eyeX, eyeY, eyeZ;
	double centerX, centerY, centerZ;

	//Tabla
	Field board[9][9];

private:
	void SetPrismMaterial(float r, float g, float b);//Dec-2011 - zad2b
	//GLRendererShapes.cpp - Start
	void DrawCube(double a);
	void DrawCuboid(double a, double b, double c);
	void DrawAxes(double len);
	void DrawGrid(double dSize, int nSteps);
	void DrawWall(double sizeX, double sizeY, int repX, int repY);
	void DrawWalls();
	void DrawBoard(double size, int numColsRows);
	void DrawCylider(double radius, double height);
	void DrawTriangularPrism(double a, double b, double angle);
	void DrawSide(double x, double y, int nPartX, int nPartY, int nTexX, int nTexY); //Jan-2018 - zad2a
	void DrawBox(double x, double y, double z, int nPartX, int nPartY, int nPartZ, int nTexX, int nTexY, int nTexZ); //Jan-2018 - zad2b
	void DrawBasket(double w, double h, double d); //Jan-2018 - zad2c
	void DrawColumn(double side, double height); //Sep-2017 - zad2a
	void DrawPyramid(double side, double height, int n); //Sep-2017 - zad2b
	void DrawDiamond(double side, double height, int n); //Sep-2017 - zad2c (Draw2Pyramid(...))
	void DrawThorus(double R, double r); //Jan-2017 - zad2a
	void RotateThorus(double L); //Jan-2017 - zad2c
	void DrawCone(double r, double h); //Apr-2013 - zad2a
	void DrawTrapezoid(double a, double b, double h, double d); //Nov-2011 - zad2a
	void DrawRing(double a, double b, double h, double d); //Nov-2011 - zad2b
	void DrawRubikCubeSingle(double a, int x, int y, int z, int count); //klk2-2014 - zad2
	void DrawPrism(float dx, float dy, float dz);//Dec-2011 - zad2a
	//GLRendererShapes.cpp - End

private:
	//GLRendererArraypoints.cpp - Start
	float vert[24];
	float col[24];
	BYTE ind[24];
	void PrepareVACube(float a);
	void DrawVACube();
	//GLRendererArraypoints.cpp - End

private:
	//GLRendererRobot.cpp - Start
	void DrawFigureShower(float w, float dx, float alpha, float beta, float gama);//Dec-2011 - zad2c
	void DrawRobot(double ugaoTrupa, double ugaoGlave, double ugaoLRamena, double ugaoDRamena, double ugaoLLakta, double ugaoDLakta);
	void DrawKer();
	void DrawHouse();
	void DrawLiftingMachine(double w, double h, double d, double dx, double alpha, double beta, double gamma);//Jan-2018 - zad2e
	void DrawDiamondOnTwine(double aS, double hS, double aR, double size, double height, double offset, double angle);//Sep-2017 - zad2d
	void DrawConnectedCones(double r, double h);//Apr-2013 - zad3
	void DrawLegoBox(double x, double y, double z);//Oct-2014 - zad2c
	void DrawLegoWall(double wallWidth);//Oct-2014 - zad2d
	void DrawRingsFllower(double a, double b, double h, double d);//Oct-2014 - zad2e
	void DrawPillar(double r, double h);//klk2-2010 - zad5
	void DrawPartenon(double r, double h);//klk2-2010 - zad6
	void DrawRubikCube(double a, int count);//klk2-2014 - zad3
	//uglovi rotacije Robota
	double m_ugaoTrupa, m_ugaoGlave;
	double m_ugaoLRamena, m_ugaoDRamena;
	double m_ugaoLLakta, m_ugaoDLakta;

	//uglovi rotacije masine za popravljanje ulicnog osvetljenja Jan-2018 - zad2e
	double m_ugaoCeleMasine, m_ugaoPrviZglob, m_ugaoDrugiZglob;
	bool m_pravacPrviZglob, m_pravacDrugiZglob;

	//ugao rotacije dijamanta
	double m_ugaoDijamanta;

	//uglovi rubikova kocka
	double m_ugaoRubikPrvaKolona;
	double m_ugaoRubikDrugaKolona;
	double m_ugaoRubikTrecaKolona;

	//GLRendererRobot.cpp - End
private:
	//Ker - Start
	double m_ugaoRep;
	bool m_pravacRepa;
	//Ker - Stop

	//Torus - Start
	bool m_pravacTorus;
	double m_pomerajTorus;
	//Torus - Stop
protected:
	HGLRC	 m_hrc; //OpenGL Rendering Context 
};
