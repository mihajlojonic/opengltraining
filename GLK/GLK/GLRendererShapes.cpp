#include "StdAfx.h"
#include "GLRenderer.h"
#include "vec3.h"
#include "GL\gl.h"
#include "GL\glu.h"
#define M_PI           3.14159265358979323846  /* pi */

//Dec-2011 - zad2a
//iscrtava cetvorostranu prizmu 
//tekstura se omotava oko strana ali ne i oko osnove
void CGLRenderer::DrawPrism(float dx, float dy, float dz)
{
	//prednja
	glBegin(GL_QUADS);
		glNormal3f(0.0, 0.0, 1.0);

		glTexCoord2f(0.0, 1.0);
		glVertex3f(-dx / 2, dy / 2, dz / 2);

		glTexCoord2f(0.0, 0.0);
		glVertex3f(-dx / 2, -dy / 2, dz / 2);

		glTexCoord2f(0.25, 0.0);
		glVertex3f(dx / 2, -dy / 2, dz / 2);

		glTexCoord2f(0.25, 1.0);
		glVertex3f(dx / 2, dy / 2, dz / 2);
	glEnd();

	//desno
	glBegin(GL_QUADS);
		glNormal3f(1.0, 0.0, 0.0);

		glTexCoord2f(0.25, 1.0);
		glVertex3f(dx / 2, dy / 2, dz / 2);

		glTexCoord2f(0.25, 0.0);
		glVertex3f(dx / 2, -dy / 2, dz / 2);

		glTexCoord2f(0.5, 0.0);
		glVertex3f(dx / 2, -dy / 2, -dz / 2);

		glTexCoord2f(0.5, 1.0);
		glVertex3f(dx / 2, dy / 2, -dz / 2);
	glEnd();

	//nazad
	glBegin(GL_QUADS);
		glNormal3f(0.0, 0.0, -1.0);

		glTexCoord2f(0.5, 1.0);
		glVertex3f(dx / 2, dy / 2, -dz / 2);

		glTexCoord2f(0.5, 0.0);
		glVertex3f(dx / 2, -dy / 2, -dz / 2);

		glTexCoord2f(0.75, 0.0);
		glVertex3f(-dx / 2, -dy / 2, -dz / 2);

		glTexCoord2f(0.75, 1.0);
		glVertex3f(-dx / 2, dy / 2, -dz / 2);
	glEnd();

	//levo
	glBegin(GL_QUADS);
		glNormal3f(-1.0, 0.0, 0.0);

		glTexCoord2f(0.75, 1.0);
		glVertex3f(-dx / 2, dy / 2, -dz / 2);

		glTexCoord2f(0.75, 0.0);
		glVertex3f(-dx / 2, -dy / 2, -dz / 2);

		glTexCoord2f(1.0, 0.0);
		glVertex3f(-dx / 2, -dy / 2, dz / 2);

		glTexCoord2f(1.0, 1.0);
		glVertex3f(-dx / 2, dy / 2, dz / 2);
	glEnd();

	//dole
	glBegin(GL_QUADS);
		glNormal3f(0.0, -1.0, 0.0);

		glVertex3f(-dx / 2, -dy / 2, dz / 2);
		glVertex3f(dx / 2, -dy / 2, dz / 2);
		glVertex3f(dx / 2, -dy / 2, -dz / 2);
		glVertex3f(-dx / 2, -dy / 2, -dz / 2);
	glEnd();

	//gore
	glBegin(GL_QUADS);
		glNormal3f(0.0,1.0,0.0);

		glVertex3f(dx / 2, dy / 2, dz / 2);
		glVertex3f(dx / 2, dy / 2, -dz / 2);
		glVertex3f(-dx / 2, dy / 2, -dz / 2);
		glVertex3f(-dx/2,dy/2,dz/2);
	glEnd();
}

//klk2-2014 - zad2
//a - duzina stranice
//prednja - crvena
//desna - plava
//zadnja - narandzasta
//leva - zelena
//gornja - bela
//donja - zuta
void CGLRenderer::DrawRubikCubeSingle(double a, int i, int j, int k, int count)
{
	double offset = 1.0 / count;

	//prednja
	matCrvena.Select();
	glBegin(GL_QUADS);
	glNormal3f(0.0, 0.0, 1.0);

	glTexCoord2f(i * offset, (count - k - 1) * offset);
	glVertex3f(-a / 2, -a / 2,     a / 2);
	
	glTexCoord2f(i * offset, (count - k) * offset);
	glVertex3f(-a / 2, a / 2,      a / 2);
	
	glTexCoord2f((i + 1) * offset, (count - k) * offset);
	glVertex3f(a / 2, a / 2,       a / 2);
	
	glTexCoord2f((i + 1) * offset, (count - k - 1) * offset);
	glVertex3f(a / 2, -a / 2,      a / 2);
	
	glEnd();

	//desna
	matPlava.Select();
	glBegin(GL_QUADS);
	glNormal3f(1.0, 0.0, 0.0);
	
	glTexCoord2f((count - j) * offset, (count - k - 1) * offset);
	glVertex3f(a / 2,    -a / 2, -a / 2);

	glTexCoord2f((count - j - 1) * offset, (count - k - 1) * offset);
	glVertex3f(a / 2,    -a / 2, a / 2);
	
	glTexCoord2f((count - j - 1) * offset, (count - k) * offset);
	glVertex3f(a / 2,     a / 2, a / 2);

	glTexCoord2f((count - j) * offset, (count - k) * offset);
	glVertex3f(a / 2,     a / 2, -a / 2);
	
	glEnd();

	//zadnja
	matNarndzasta.Select();
	glBegin(GL_QUADS);
	glNormal3f(0.0, 0.0, -1.0);

	glTexCoord2f(i * offset, (k + 1) * offset);
	glVertex3f(-a / 2, -a / 2,     -a / 2);
	
	glTexCoord2f((i + 1) * offset, (k + 1) * offset);
	glVertex3f(a / 2, -a / 2,      -a / 2);

	glTexCoord2f((i + 1) * offset, (k) * offset);
	glVertex3f(a / 2, a / 2,       -a / 2);

	glTexCoord2f(i * offset, (k) * offset);
	glVertex3f(-a / 2, a / 2,      -a / 2);
	glEnd();

	//leva
	matZelena.Select();
	glBegin(GL_QUADS);
	glNormal3f(-1.0, 0.0, 0.0);

	glTexCoord2f((count - j) * offset, (count - k - 1) * offset);
	glVertex3f(-a / 2,      -a / 2, -a / 2);

	glTexCoord2f((count - j) * offset, (count - k) * offset);
	glVertex3f(-a / 2,       a / 2, -a / 2);

	glTexCoord2f((count - j - 1) * offset, (count - k) * offset);
	glVertex3f(-a / 2,       a / 2, a / 2);
	
	glTexCoord2f((count - j - 1) * offset, (count - k - 1) * offset);
	glVertex3f(-a / 2,       -a / 2, a / 2);
	glEnd();

	//gornja
	matBela.Select();
	glBegin(GL_QUADS);
	glNormal3f(0.0, 1.0, 0.0);

	glTexCoord2f(i * offset, (count - j) * offset);
	glVertex3f(-a / 2, a / 2, -a / 2);

	glTexCoord2f((i + 1) * offset, (count - j) * offset);
	glVertex3f(a / 2, a / 2, -a / 2);

	glTexCoord2f((i + 1) * offset, (count - j - 1) * offset);
	glVertex3f(a / 2, a / 2, a / 2);
	
	glTexCoord2f(i * offset, (count - j - 1) * offset);
	glVertex3f(-a / 2, a / 2, a / 2);

	glEnd();

	//donja
	matZuta.Select();
	glBegin(GL_QUADS);
	glNormal3f(0.0, -1.0, 0.0);

	glTexCoord2f(i * offset, j * offset);
	glVertex3f(-a / 2, -a / 2, -a / 2);

	glTexCoord2f(i * offset, (j + 1) * offset);
	glVertex3f(-a / 2, -a / 2, a / 2);
	
	glTexCoord2f((i + 1) * offset, (j + 1) * offset);
	glVertex3f(a / 2, -a / 2, a / 2);
	
	glTexCoord2f((i + 1) * offset, j * offset);
	glVertex3f(a / 2, -a / 2, -a / 2);
	glEnd();
}

//Nov-2011 - zad2a////////////////////////////////////////////////////////////////////////////////////////////
//a - duzina donje osnove
//b - duzina gornje osnove
//h - visina trapezoida
//d - duzina trapezoida
void CGLRenderer::DrawTrapezoid(double a, double b, double h, double d)
{
	//dole
	glBegin(GL_QUADS);
		glNormal3f(0.0, -1.0, 0.0);

		glTexCoord2f(0.0, 0.0);
		glVertex3f(-a / 2, 0.0, d / 2);
		
		glTexCoord2f(1.0, 0.0);
		glVertex3f(a / 2, 0.0, d / 2);
		
		glTexCoord2f(1.0, 1.0);
		glVertex3f(a / 2, 0.0, -d / 2);
		
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-a / 2, 0.0, -d / 2);
	glEnd();

	//gore
	glBegin(GL_QUADS);
		glNormal3f(0.0, 1.0, 0.0);

		glTexCoord2f(0.0, 0.0);
		glVertex3f(-b / 2, h, d / 2);

		glTexCoord2f(1.0, 0.0);
		glVertex3f(b / 2, h, d / 2);

		glTexCoord2f(1.0, 1.0);
		glVertex3f(b / 2, h, -d / 2);

		glTexCoord2f(0.0, 1.0);
		glVertex3f(-b / 2, h, -d / 2);
	glEnd();

	//omotac
	glBegin(GL_QUAD_STRIP);
		vec3 norm = vec3::getNormal(vec3(-a / 2, 0.0, d / 2), vec3(-b / 2, h, d / 2));
		
		glNormal3f(norm.x, norm.y, norm.z);
		glTexCoord2f(0.25, 0.0);
		glVertex3f(-a / 2, 0.0, d / 2);
		glTexCoord2f(0.25, 1.0);
		glVertex3f(-b / 2, h, d / 2);

		norm = vec3::getNormal(vec3(a / 2, 0.0, d / 2), vec3(b / 2, h, d / 2));

		glNormal3f(norm.x, norm.y, norm.z);
		glTexCoord2f(0.5, 0.0);
		glVertex3f(a / 2, 0.0, d / 2);
		glTexCoord2f(0.5, 1.0);
		glVertex3f(b / 2, h, d / 2);

		norm = vec3::getNormal(vec3(a / 2, 0.0, -d / 2), vec3(b / 2, h, -d / 2));

		glNormal3f(norm.x, norm.y, norm.z);
		glTexCoord2f(1.0, 0.0);
		glVertex3f(a / 2, 0.0, -d / 2);
		glTexCoord2f(1.0, 1.0);
		glVertex3f(b / 2, h, -d / 2);

		norm = vec3::getNormal(vec3(-a / 2, 0.0, -d / 2), vec3(-b / 2, h, -d / 2));

		glNormal3f(norm.x, norm.y, norm.z);
		glTexCoord2f(0.0, 0.0);
		glVertex3f(-a / 2, 0.0, -d / 2);
		glTexCoord2f(0.0, 1.0);
		glVertex3f(-b / 2, h, -d / 2);

		norm = vec3::getNormal(vec3(-a / 2, 0.0, d / 2), vec3(-b / 2, h, d / 2));

		glNormal3f(norm.x, norm.y, norm.z);
		glTexCoord2f(0.75, 0.0);
		glVertex3f(-a / 2, 0.0, d / 2);
		glTexCoord2f(0.75, 1.0);
		glVertex3f(-b / 2, h, d / 2);
	glEnd();

}

//Apr-2013 - zad2a//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//r - poluprecnik osnove 
//h - visina kupe
void CGLRenderer::DrawCone(double r, double h)
{
	double x;
	double z;
	double angle = 0.0;
	
	//osnova
	glBegin(GL_TRIANGLE_FAN);
	glNormal3f(0.0, -1.0, 0.0);
	while (angle < 2 * M_PI + 0.1)
	{
		x = r * cos(angle);
		z = r * sin(angle);
		glVertex3f(x, 0.0, z);

		angle += 0.1;
	}
	glEnd();

	//kapa :D
	angle = 0.0;
	glBegin(GL_TRIANGLE_FAN);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(0.0, h, 0.0);
	while (angle < 2 * M_PI + 0.1)
	{
		x = r * cos(angle);
		z = r * sin(angle);
	
		vec3 norm = vec3::getNormal(vec3(x, 0.0, z), vec3(0.0, h, 0.0));

		glNormal3f(norm.x, norm.y, norm.z);
		glTexCoord2f(angle / 2 * M_PI, 0.0);
		glVertex3f(x, 0.0, z);

		angle += 0.1;
	}
	glEnd();
}

//Jan-2017 - zad2c///////////////////////////////////////////////////////////////////////////////////////
//rotira torus duz x ose za L duzinu
void CGLRenderer::RotateThorus(double L)
{
	glTranslatef(-10.0, -14.0, 10.0);

	double R = 5.0;
	double r = 1.0;

	double obim = 2 * (R + r/2) * M_PI;

	double angle = 360 * L / obim;

	glTranslatef(L, 0.0, 0.0);
	glRotatef(angle, 0.0, 0.0, 1.0);

	DrawThorus(R, r);
}

//Jan-2017 - zad2a
//r - poluprecnik poprecnog preseka
//R - rastojanje centara cevi od centra torusa
void CGLRenderer::DrawThorus(double R, double r)
{
	int numTubes = 8;
	int numBlcoksPerTube = 25;
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	double angleTorus = 0.0;
	double angleTube = 0.0;
	int tex = 0;
	for (int i = 0; i < numTubes; i++)
	{
		glBegin(GL_QUAD_STRIP);
		for (int j = 0; j <= numBlcoksPerTube; j++)
		{
			for (int k = 1; k >= 0; k--)
			{
				angleTube = (i + k) % numTubes;
				angleTorus = j % numBlcoksPerTube;

				x = (R + r * cos(angleTube * 2 * M_PI / numTubes)) * cos(angleTorus * 2 * M_PI / numBlcoksPerTube);
				y = (R + r * cos(angleTube * 2 * M_PI / numTubes)) * sin(angleTorus * 2 * M_PI / numBlcoksPerTube);
				z = r * sin(angleTube * 2 * M_PI / numTubes);

				if(tex == 0)
					glTexCoord2f(0.0, 0.0);
				else if(tex == 1)
					glTexCoord2f(0.0, 1.0);
				else if(tex == 2)
					glTexCoord2f(1.0, 0.0);
				else
					glTexCoord2f(1.0, 1.0);

				tex = (tex + 1) % 4;

				glVertex3f(x, y, z);
			}
		}
		glEnd();
	}
}

//Sep-2017 - zad2c (Draw2Pyramid(...))
//spoj 2 piramide po osnovama
void CGLRenderer::DrawDiamond(double side, double height, int n)
{
	DrawPyramid(side, height, n);
	glPushMatrix();
		glRotatef(180.0, 1.0, 0.0, 0.0);
		DrawPyramid(side, height, n);
	glPopMatrix();
}

//Sep-2017 - zad2b /////////////////////////////////////////////////////////////////////////////////////////////
//piramida sa n-touglom u osnovi sa duzinom stranice side i visinom height
void CGLRenderer::DrawPyramid(double side, double height, int n)
{
	if (n < 3) return;

	double tmpAngle = 360.0 / n;
	double radius = side / (2 * sin(tmpAngle / 2));
	
	double angle = 0.0;
	glBegin(GL_TRIANGLE_FAN);
		glNormal3f(0.0, -1.0, 0.0);
		for (int i = 0; i < n; i++)
		{
			double x = radius * cos(angle*M_PI / 180.0);
			double z = radius * sin(angle*M_PI / 180.0);
		
			glVertex3f(x, 0.0, z);

			angle += tmpAngle;
		}
	glEnd();
	
	angle = 0.0;
	glBegin(GL_TRIANGLE_FAN);
		glVertex3f(0.0, height, 0.0);
		for (int i = 0; i < n; i++)
		{
			double x = radius * cos(angle*M_PI / 180.0);
			double z = radius * sin(angle*M_PI / 180.0);

			vec3 norm = vec3::getNormal(vec3(0.0, height, 0.0), vec3(x, 0.0, z));
			glNormal3f(norm.x, norm.y, norm.z);
			glVertex3f(x, 0.0, z);

			angle += tmpAngle;
		}
	glEnd();
}

//Sep-2017 - zad2a //////////////////////////////////////////////////////////////////////////
//stub cija je osnova kvadrat stranice 'side' i visne 'height'
void CGLRenderer::DrawColumn(double side, double height)
{
	//dole
	glPushMatrix();
		glTranslatef(0.0, -height / 2, 0.0);
		
		glBegin(GL_QUADS);
		glNormal3f(0.0, -1.0, 0.0);

		glTexCoord2f(0.0, 1.0);
		glVertex3f(-side / 2, 0.0, side / 2);

		glTexCoord2f(1.0, 1.0);
		glVertex3f(side / 2, 0.0, side / 2);

		glTexCoord2f(1.0, 0.0);		
		glVertex3f(side / 2, 0.0, -side / 2);

		glTexCoord2f(0.0, 0.0);
		glVertex3f(-side / 2, 0.0, -side / 2);
		glEnd();
	glPopMatrix();

	//gore
	glPushMatrix();
		glTranslatef(0.0, height / 2, 0.0);

		glBegin(GL_QUADS);
		glNormal3f(0.0, 1.0, 0.0);

		glTexCoord2f(0.0, 1.0);
		glVertex3f(-side / 2, 0.0, side / 2);

		glTexCoord2f(1.0, 1.0);
		glVertex3f(side / 2, 0.0, side / 2);
		
		glTexCoord2f(1.0, 0.0);
		glVertex3f(side / 2, 0.0, -side / 2);

		glTexCoord2f(0.0, 0.0);
		glVertex3f(-side / 2, 0.0, -side / 2);
		glEnd();
	glPopMatrix();

	//napred
	glPushMatrix();
		glTranslated(0.0, 0.0, side / 2);

		glBegin(GL_QUADS);
		glNormal3f(0.0, 0.0, 1.0);

		glTexCoord2f(0.0, 0.0);
		glVertex3f(-side / 2, -height / 2, 0.0);

		glTexCoord2f(1.0, 0.0);
		glVertex3f(side / 2, -height / 2, 0.0);

		glTexCoord2f(1.0, 1.0);
		glVertex3f(side / 2, height / 2, 0.0);

		glTexCoord2f(0.0, 1.0);
		glVertex3f(-side / 2, height / 2, 0.0);
		glEnd();
	glPopMatrix();

	//nazad
	glPushMatrix();
		glTranslated(0.0, 0.0, -side / 2);

		glBegin(GL_QUADS);
		glNormal3f(0.0, 0.0, -1.0);

		glTexCoord2f(0.0, 0.0);
		glVertex3f(-side / 2, -height / 2, 0.0);

		glTexCoord2f(1.0, 0.0);
		glVertex3f(side / 2, -height / 2, 0.0);

		glTexCoord2f(1.0, 1.0);
		glVertex3f(side / 2, height / 2, 0.0);

		glTexCoord2f(0.0, 1.0);
		glVertex3f(-side / 2, height / 2, 0.0);
		glEnd();
	glPopMatrix();

	//levo
	glPushMatrix();
		glTranslatef(-side / 2, 0.0, 0.0);

		glBegin(GL_QUADS);
		glNormal3f(-1.0, 0.0, 0.0);

		glTexCoord2f(0.0, 0.0);
		glVertex3f(0.0, -height / 2, -side / 2);

		glTexCoord2f(0.0, 1.0);
		glVertex3f(0.0, -height / 2, side / 2);

		glTexCoord2f(1.0, 1.0);
		glVertex3f(0.0, height / 2, side / 2);

		glTexCoord2f(1.0, 0.0);
		glVertex3f(0.0, height / 2, -side / 2);
		glEnd();
	glPopMatrix();

	//desno
	glPushMatrix();
		glTranslatef(side / 2, 0.0, 0.0);

		glBegin(GL_QUADS);
		glNormal3f(1.0, 0.0, 0.0);

		glTexCoord2f(0.0, 0.0);
		glVertex3f(0.0, -height / 2, -side / 2);

		glTexCoord2f(0.0, 1.0);
		glVertex3f(0.0, -height / 2, side / 2);

		glTexCoord2f(1.0, 1.0);
		glVertex3f(0.0, height / 2, side / 2);

		glTexCoord2f(1.0, 0.0);
		glVertex3f(0.0, height / 2, -side / 2);
		glEnd();
	glPopMatrix();

}

//Jan-2018 - zad2c///////////////////////////////////////////////////////////////////////////
//korpa oblika kvadra dimenzija w*h*w, deblina zidova d
//po w se deli na 10 delova a po h na 20 delova, dok se po d ne deli
void CGLRenderer::DrawBasket(double w, double h, double d)
{
	//nazad
	glPushMatrix();
	m_tex2.Select();
	glTranslatef(0.0, 0.0, -w / 2 + d / 2);
	DrawBox(w - 2 * d, h - d, d, 10, 20, 1, 1, 1, 1);
	glPopMatrix();

	//dole
	glPushMatrix();
		m_tex1.Select();
		glTranslatef(0.0, -h/2/* + d / 2*/, 0.0);
		DrawBox(w, d, w, 10, 1, 10, 1, 1, 1);
	glPopMatrix();

	//levo
	glPushMatrix();
		m_tex3.Select();
		glTranslatef(-w/2 + d / 2, 0.0, 0.0);
		DrawBox(d, h - d, w, 1, 20, 10, 1, 1, 1);
	glPopMatrix();

	//desno
	glPushMatrix();
		glTranslatef(w/2 - d / 2, 0.0, 0.0);
		DrawBox(d, h - d, w, 1, 20, 10, 1, 1, 1);
	glPopMatrix();

	//napred
	glPushMatrix();
		m_white_tex.Select();
		glTranslatef(0.0, 0.0, w / 2 - d / 2);
		DrawBox(w - 2 * d, h - d, d, 10, 20, 1, 1, 1, 1);
	glPopMatrix();

	
}

//Jan-2018 - zad2a /////////////////////////////////////////////////////////////////////////////
//kvadar dimenzija x*y*z
//podele strana nPartX, nPartY, nPartZ
//ponavljanje teksture nTexX, nTexY, nTexZ
void CGLRenderer::DrawBox(double x, double y, double z, int nPartX, int nPartY, int nPartZ, int nTexX, int nTexY, int nTexZ)
{
	//nazad
	glPushMatrix();
	glTranslated(0.0, 0.0, -z / 2);
	glRotatef(-90.0, 1.0, 0.0, 0.0);
	DrawSide(x, y, nPartX, nPartY, nTexX, nTexY);
	glPopMatrix();

	//dole
	glPushMatrix();
		glTranslatef(0.0, -y / 2, 0.0);
		glRotatef(180.0, 0.0, 0.0, 1.0);
		DrawSide(x, z, nPartX, nPartZ, nTexX, nTexZ);
	glPopMatrix();

	//levo
	glPushMatrix();
		glTranslatef(-x / 2, 0.0, 0.0);
		glRotatef(-90.0, 0.0, 0.0, 1.0);
		DrawSide(y, z, nPartY, nPartZ, nTexY, nTexZ);
	glPopMatrix();

	//desno
	glPushMatrix();
		glTranslatef(x / 2, 0.0, 0.0);
		glRotatef(90.0, 0.0, 0.0, 1.0);
		DrawSide(y, z, nPartY, nPartZ, nTexY, nTexZ);
	glPopMatrix();

	//gore
	glPushMatrix();
	glTranslatef(0.0, y / 2, 0.0);
	DrawSide(x, z, nPartX, nPartZ, nTexX, nTexZ);
	glPopMatrix();

	//napred
	glPushMatrix();
	glTranslatef(0.0, 0.0, z / 2);
	glRotatef(90.0, 1.0, 0.0, 0.0);
	DrawSide(x, y, nPartX, nPartY, nTexX, nTexY);
	glPopMatrix();	
}

//Jan-2018 - zad2a //////////////////////////////////////////////////////////////////////////////
//pravougaonik vlicine x*y
//podeljen na nPartX po x i nPartY po y
//ponavljanje teksture nTexX po x i nTexY po y
void CGLRenderer::DrawSide(double x, double y, int nPartX, int nPartY, int nTexX, int nTexY)
{
	double partXLen = x / nPartX;
	double partYLen = y / nPartY;
	glPushMatrix();
	glTranslatef(-x / 2, 0.0, -y / 2);
	for (int i = 0; i < nPartX; i++)
	{
		for (int j = 0; j < nPartY; j++)
		{
			glBegin(GL_QUADS);
				glNormal3f(0.0, 1.0, 0.0);

				glTexCoord2f(0.0, 0.0);
				glVertex3f(i*partXLen, 0.0, (j+1)*partYLen);
				
				glTexCoord2f(nTexX, 0.0);
				glVertex3f((i+1)*partXLen, 0.0, (j + 1)*partYLen);
				
				glTexCoord2f(nTexX, nTexY);
				glVertex3f((i+1)*partXLen, 0.0, j*partYLen);
				
				glTexCoord2f(0.0, nTexY);
				glVertex3f(i*partXLen, 0.0, j*partYLen);
			glEnd();
		}
	}
	glPopMatrix();
}

/////////////////////////////////////////////////////////////////////////////////
void CGLRenderer::DrawTriangularPrism(double a, double b, double angle)
{
	glPushMatrix();
		glBegin(GL_QUADS);
			//plafon
			glNormal3f(0.0, 1.0, 0.0);
			glVertex3f(-a / 2, 0.0,  b / 2);
			glVertex3f( a / 2, 0.0,  b / 2);
			glVertex3f( a / 2, 0.0, -b / 2);
			glVertex3f(-a / 2, 0.0, -b / 2);
		glEnd();
		
		m_tex1.Select();
		glBegin(GL_TRIANGLES);
			glNormal3f(0.0, 0.0, 1.0);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(-a / 2, 0.0, b / 2);
			glTexCoord2f(0.0, 1.0);
			glVertex3f(a / 2, 0.0, b / 2);
			glTexCoord2f(0.5, 0.5);
			glVertex3f(0.0, a / (2 * tan(angle / 2)), b / 2);
		glEnd();

		glBegin(GL_TRIANGLES);
			glNormal3f(0.0, 0.0, -1.0);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(a / 2, 0.0, -b / 2);
			glTexCoord2f(0.0, 1.0);
			glVertex3f(-a / 2, 0.0, -b / 2);
			glTexCoord2f(0.5, 0.5);
			glVertex3f(0.0, a / (2 * tan(angle / 2)), -b / 2);
		glEnd();
		
		m_tex3.Select();
		glBegin(GL_QUADS);
			vec3 norm = vec3::getNormal(vec3(-a / 2, 0.0, b / 2), vec3(-a / 2, 0.0, -b / 2));
			
			glNormal3f(norm.x, norm.y, norm.z);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(-a / 2, 0.0, b / 2);

			glTexCoord2f(0.0, 1.0);
			glVertex3f(0.0, a / (2 * tan(angle / 2)), b / 2);

			glTexCoord2f(1.0, 0.0);
			glVertex3f(0.0, a / (2 * tan(angle / 2)), -b / 2);

			glTexCoord2f(1.0, 1.0);
			glVertex3f(-a / 2, 0.0, -b / 2);
		glEnd();

		glBegin(GL_QUADS);
			norm = vec3::getNormal(vec3(a / 2, 0.0, -b / 2), vec3(a / 2, 0.0, b / 2));

			glNormal3f(norm.x, norm.y, norm.z);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(0.0, a / (2 * tan(angle / 2)), b / 2);
		
			glTexCoord2f(0.0, 1.0);
			glVertex3f(a / 2, 0.0, b / 2);
		
			glTexCoord2f(1.0, 0.0);
			glVertex3f(a / 2, 0.0, -b / 2);
			
			glTexCoord2f(1.0, 1.0);
			glVertex3f(0.0, a / (2 * tan(angle / 2)), -b / 2);
		glEnd();	
	glPopMatrix();
}

//////////////////////////////////////////////////////////////////////////
void CGLRenderer::DrawWalls()
{
	m_tex1.Select();
	glPushMatrix();
		glTranslatef(0.0, 0.0, -20.0);
		DrawWall(40.0, 40.0, 4, 4);
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(-20, 0.0, 0.0);
		glRotatef(90.0, 0.0, 1.0, 0.0);
		DrawWall(40.0, 40.0, 4, 4);
	glPopMatrix();
	
	m_tex2.Select();
	glPushMatrix();
		glTranslatef(0.0, -20.0, 0.0);
		glRotatef(-90.0, 1.0, 0.0, 0.0);
		DrawWall(40.0, 40.0, 4, 4);
	glPopMatrix();	
}

///////////////////////////////////////////
void CGLRenderer::DrawWall(double sizeX, double sizeY, int repX, int repY)
{
	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 0.0);
		glNormal3f(0.0, 0.0, 1.0);
		glVertex3f(-(sizeX / 2), -(sizeY / 2), 0.0); // -1, -1

		glTexCoord2f(repX, 0.0);
		glVertex3f((sizeX / 2), -(sizeY / 2), 0.0); //   1, -1

		glTexCoord2f(repX, repY);
		glVertex3f((sizeX / 2), (sizeY / 2), 0.0); //    1,  1

		glTexCoord2f(0.0, repY);
		glVertex3f(-(sizeX / 2), (sizeY / 2), 0.0); //  -1,  1
	glEnd();
}

/////////////////////////////////////////
void CGLRenderer::DrawCube(double a)
{
	glBegin(GL_QUADS);
	//Prednja stranica
	glNormal3f(0.0, 0.0, 1.0);
	glTexCoord2f(0.0, 1.0);
	glVertex3d(-a / 2, a / 2, a / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3d(-a / 2, -a / 2, a / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3d(a / 2, -a / 2, a / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3d(a / 2, a / 2, a / 2);

	//Desna stranica
	glNormal3f(1.0, 0.0, 0.0);
	glTexCoord2f(0.0, 1.0);
	glVertex3d(a / 2, a / 2, a / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3d(a / 2, -a / 2, a / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3d(a / 2, -a / 2, -a / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3d(a / 2, a / 2, -a / 2);
	
	//Zadnja stranica
	glNormal3f(0.0, 0.0, -1.0);
	glTexCoord2f(0.0, 1.0);
	glVertex3d(a / 2, a / 2, -a / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3d(a / 2, -a / 2, -a / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3d(-a / 2, -a / 2, -a / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3d(-a / 2, a / 2, -a / 2);
	
	//Leva stranica
	glNormal3f(-1.0, 0.0, 0.0);
	glTexCoord2f(0.0, 1.0);
	glVertex3d(-a / 2, a / 2, -a / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3d(-a / 2, -a / 2, -a / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3d(-a / 2, -a / 2, a / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3d(-a / 2, a / 2, a / 2);

	//Gornja stranica
	glNormal3f(0.0, 1.0, 0.0);//////////
	glTexCoord2f(0.0, 0.0);
	glVertex3d(-a / 2, a / 2, a / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3d(a / 2, a / 2, a / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3d(a / 2, a / 2, -a / 2);
	glTexCoord2f(0.0, 1.0);
	glVertex3d(-a / 2, a / 2, -a / 2);

	//Donja stranica
	glNormal3f(0.0, 1.0, 0.0);///////////////////////////
	glTexCoord2f(0.0, 0.0);
	glVertex3d(-a / 2, -a / 2, -a / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3d(a / 2, -a / 2, -a / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3d(a / 2, -a / 2, a / 2);
	glTexCoord2f(0.0, 1.0);
	glVertex3d(-a / 2, -a / 2, a / 2);

	glEnd();
}

/////////////////////////////////////////
void CGLRenderer::DrawCuboid(double a, double b, double c)
{
	int repX = 2;
	int repY = 2;

	glBegin(GL_QUADS);

	//Prednja stranica
	glNormal3f(0.0, 0.0, 1.0);
	glTexCoord2f(0.0, repY);
	glVertex3d(-a / 2, b / 2, c / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3d(-a / 2, -b / 2, c / 2);
	glTexCoord2f(repX, 0.0);
	glVertex3d(a / 2, -b / 2, c / 2);
	glTexCoord2f(repX, repY);
	glVertex3d(a / 2, b / 2, c / 2);

	//Desna stranica
	glNormal3f(1.0, 0.0, 0.0);
	glTexCoord2f(0.0, repY);
	glVertex3d(a / 2, b / 2, c / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3d(a / 2, -b / 2, c / 2);
	glTexCoord2f(repX, 0.0);
	glVertex3d(a / 2, -b / 2, -c / 2);
	glTexCoord2f(repX, repY);
	glVertex3d(a / 2, b / 2, -c / 2);

	//Zadnja stranica
	glNormal3f(0.0, 0.0, -1.0);
	glTexCoord2f(0.0, repY);
	glVertex3d(a / 2, b / 2, -c / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3d(a / 2, -b / 2, -c / 2);
	glTexCoord2f(repX, 0.0);
	glVertex3d(-a / 2, -b / 2, -c / 2);
	glTexCoord2f(repX, repY);
	glVertex3d(-a / 2, b / 2, -c / 2);
	
	//Leva stranica
	glNormal3f(-1.0, 0.0, 0.0);
	glTexCoord2f(0.0, repY);
	glVertex3d(-a / 2, b / 2, -c / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3d(-a / 2, -b / 2, -c / 2);
	glTexCoord2f(repX, 0.0);
	glVertex3d(-a / 2, -b / 2, c / 2);
	glTexCoord2f(repX, repY);
	glVertex3d(-a / 2, b / 2, c / 2);

	//Gornja stranica
	glNormal3f(0.0, 1.0, 0.0);
	glTexCoord2f(0.0, 0.0);
	glVertex3d(-a / 2, b / 2, c / 2);
	glTexCoord2f(repX, 0.0);
	glVertex3d(a / 2, b / 2, c / 2);
	glTexCoord2f(repX, repY);
	glVertex3d(a / 2, b / 2, -c / 2);
	glTexCoord2f(0.0, repY);
	glVertex3d(-a / 2, b / 2, -c / 2);

	//Donja stranica
	glNormal3f(0.0, 1.0, 0.0);/////////////////////////////////////
	glTexCoord2f(0.0, 0.0);
	glVertex3d(-a / 2, -b / 2, -c / 2);
	glTexCoord2f(repX, 0.0);
	glVertex3d(a / 2, -b / 2, -c / 2);
	glTexCoord2f(repX, repY);
	glVertex3d(a / 2, -b / 2, c / 2);
	glTexCoord2f(0.0, repY);
	glVertex3d(-a / 2, -b / 2, c / 2);

	glEnd();
}

/////////////////////////////////////////
void CGLRenderer::DrawAxes(double len)
{
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(-len, 0.0, 0.0);
	glVertex3f(len, 0.0, 0.0);

	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(0.0, len, 0.0);
	glVertex3f(0.0, /*-len*/0.0, 0.0);

	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.0, 0.0, -len);
	glVertex3f(0.0, 0.0, len);
	glEnd();
}

/////////////////////////////////////////
void CGLRenderer::DrawGrid(double dSize, int nSteps)
{
	glBegin(GL_LINES);
	glColor3f(0.0, 0.0, 0.0);

	double stepSize = dSize / nSteps;
	for (int i = 0; i < 2 * nSteps; i++)
	{
		glVertex3f(-dSize + i * stepSize, 0.0, -dSize);
		glVertex3f(-dSize + i * stepSize, 0.0, dSize);

		glVertex3f(-dSize, 0.0, -dSize + i * stepSize);
		glVertex3f(dSize, 0.0, -dSize + i * stepSize);
	}

	glEnd();
}

/////////////////////////////////////////
void CGLRenderer::DrawBoard(double size, int numColsRows)
{
	glEnable(GL_TEXTURE_2D);
	glTranslatef(-size / 2, -19.0, -size / 2);//
	/*
	m_grass_tex.Select();
	glBegin(GL_QUADS);
		glNormal3f(0.0, 1.0, 0.0);
		
		glTexCoord2f(0.0, 1.0);
		glVertex3f(0.0, 0.0, 0.0);
		
		glTexCoord2f(0.0, 0.0);
		glVertex3f(0.0, 0.0, size);
		
		glTexCoord2f(1.0, 0.0);
		glVertex3f(size, 0.0, size);
		
		glTexCoord2f(1.0, 1.0);
		glVertex3f(size, 0.0, 0.0);
	glEnd();

	*/
	double fieldLen = size / numColsRows;
	/*
	glBegin(GL_LINES);
		for (int i = 0; i < numColsRows; i++)
		{
			glVertex3f(fieldLen * i, 0.0, 0.0);
			glVertex3f(fieldLen * i, 0.0, size);

			glVertex3f(0.0, 0.0, fieldLen * i);
			glVertex3f(size, 0.0, fieldLen * i);
		}
	glEnd();*/

	glTranslatef(0.0, 0.1, 0.0);
	
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (board[i][j] == Field::W)
			{
				m_black_tex.Select();
				glPushMatrix();
					glTranslatef(j * fieldLen + fieldLen / 2, 0.0, i * fieldLen + fieldLen / 2);
					DrawCylider(fieldLen / 2, 0.5);
				glPopMatrix();
			}
			else if (board[i][j] == Field::B)
			{
				//matCube.Select();
				m_white_tex.Select();
				glPushMatrix();
					glTranslatef(j * fieldLen + fieldLen / 2, 0.0, i * fieldLen + fieldLen / 2);
					DrawCylider(fieldLen / 2, 0.5);
				glPopMatrix();
			}
		}
	}
	glDisable(GL_TEXTURE_2D);


//	glTranslatef(fieldLen / 2, 0.1, fieldLen / 2);
	//DrawCylider(fieldLen / 2, 0.5);
}

void CGLRenderer::DrawCylider(double radius, double height)
{
	double x = 0.0;
	double y = 0.0;
	double angle = 0.0;
	double angleStep = 0.1;

	//draw the tube
	matCube.Select();
	glBegin(GL_QUAD_STRIP);

	while (angle < 2*M_PI)
	{
		x = radius * cos(angle);
		y = radius * sin(angle);

		vec3 norm = vec3::getNormal(vec3(x, height, y), vec3(x, 0.0, y));

		glNormal3f(norm.x, norm.y, norm.z);

		glTexCoord2f(angle/2*M_PI, 1.0);
		glVertex3f(x, height, y);
		glTexCoord2f(angle / 2 * M_PI, 0.0);
		glVertex3f(x, 0.0, y);

		angle = angle + angleStep;
	}

	glVertex3f(radius, height, 0.0);
	glVertex3f(radius, 0.0, 0.0);

	glEnd();

	glBegin(GL_POLYGON);
	angle = 0.0;
	glNormal3f(0.0, 1.0, 0.0);
	while (angle < 2 * M_PI) {
		x = radius * cos(angle);
		y = radius * sin(angle);

		

		glVertex3f(x, height, y);
		angle = angle + angleStep;
	}
	glVertex3f(radius, height, 0.0);
	glEnd();
}
