#pragma once
class CGLTexture
{
private:
	UINT m_ID;

public:
	CGLTexture();
	virtual ~CGLTexture();

	void LoadFromFile(CString texFileName);
	void Select(void);
	void Relese(void);

	static void PrepareTexturing(bool bEnableLighting);
};

