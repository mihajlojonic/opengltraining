#include "stdafx.h"
#include "CGLTexture.h"
#include "GL\GLAUX.H"

CGLTexture::CGLTexture()
{
}


CGLTexture::~CGLTexture()
{
}

void CGLTexture::LoadFromFile(CString texFileName)
{
	if (m_ID != 0) Relese();

	//Alokacija ID-a i kreiranje teksture
	glGenTextures(1, &m_ID);
	glBindTexture(GL_TEXTURE_2D, m_ID);

	//Definicije parametara teksture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	//Ucitavanje bitmape
	AUX_RGBImageRec* TextureImage;
	TextureImage = auxDIBImageLoad(texFileName);

	//Kopiranje sadrzaja bitmape u teksturu
	glTexImage2D(GL_TEXTURE_2D, 0, 3, TextureImage->sizeX, TextureImage->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, TextureImage->data);
	
	//Brisanje bitmape
	if (TextureImage) // ako je ucitana slika
	{
		if (TextureImage->data) // i ako sadrzi podatke
			free(TextureImage->data); // obrisati podatke
		free(TextureImage); // obrisati samu strukture
	}
}

void CGLTexture::Select()
{
	if (m_ID)
		glBindTexture(GL_TEXTURE_2D, m_ID);
}

void CGLTexture::Relese()
{
	if (m_ID)
	{
		glDeleteTextures(1, &m_ID);
		m_ID = 0;
	}
}

void CGLTexture::PrepareTexturing(bool bEnableLighting)
{
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	
	if (bEnableLighting)
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	else
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}
