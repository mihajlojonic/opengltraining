#include "StdAfx.h"
#include "GLRenderer.h"
#include "GL\gl.h"
#include "GL\glu.h"
#include "GL\GLAUX.H"
#include "GL\glut.h"
#include "CGLTexture.h"
//#include "BoardState.h"

#pragma comment(lib, "legacy_stdio_definitions.lib")
#pragma comment(lib, "GL\\glut32.lib")
#pragma comment(lib, "GL\\GLAUX.LIB")
#pragma comment(lib, "GL\\GLU32.LIB")


CGLRenderer::CGLRenderer(void)
{
	m_ugaoTrupa = m_ugaoGlave = m_ugaoLRamena = m_ugaoDRamena = m_ugaoLLakta = m_ugaoDLakta = 0.0;
	eyeX = 3.0;
	eyeY = 26.0;
	eyeZ = 14.0;

	centerX = 0.0;
	centerY = 0.0;
	centerZ = 0.0;

	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++) 
		{
			if (i < 2) board[i][j] = Field::B;
			else if (i > 6) board[i][j] = Field::W;
			else board[i][j] = Field::E;
		}
	}

	//ker
	m_ugaoRep = 0.0;
	m_pravacRepa = true;

	//masina za popravljanje osvetljenja
	m_ugaoCeleMasine = 0.0;
	m_ugaoPrviZglob = 30.0;
	m_ugaoDrugiZglob = -20.0;

	m_pravacPrviZglob = true;
	m_pravacDrugiZglob = true;

	//dijamant na kanapu
	m_ugaoDijamanta = 0.0;

	//pomeranje (kotrljanje) torusa
	m_pravacTorus = true;
	m_pomerajTorus = 0.0;

	//uglovi rubikova kokca
	m_ugaoRubikPrvaKolona = 0.0;
	m_ugaoRubikDrugaKolona = 0.0;
	m_ugaoRubikTrecaKolona = 0.0;
}

CGLRenderer::~CGLRenderer(void)
{
}

bool CGLRenderer::CreateGLContext(CDC* pDC)
{
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 24; //32
	pfd.iLayerType = PFD_MAIN_PLANE;

	int nPixelFormat = ChoosePixelFormat(pDC->m_hDC, &pfd);

	if (nPixelFormat == 0) return false;

	BOOL bResult = SetPixelFormat(pDC->m_hDC, nPixelFormat, &pfd);

	if (!bResult) return false;

	m_hrc = wglCreateContext(pDC->m_hDC);

	if (!m_hrc) return false;

	return true;
}

void CGLRenderer::PrepareScene(CDC *pDC)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	//---------------------------------
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glEnable(GL_DEPTH_TEST);
	PrepareLighting();
	PrepareMaterials();
	
	glEnable(GL_TEXTURE_2D);
	CGLTexture::PrepareTexturing(false); //ASHSEN512.bmp
	m_tex1.LoadFromFile(_T("E:\\grafika\\Teksture\\Bricks_512x512.bmp")); //cigla zid
	m_tex2.LoadFromFile(_T("E:\\grafika\\Teksture\\PAT39.BMP")); //pod
	m_tex3.LoadFromFile(_T("E:\\grafika\\Teksture\\Floor_Wdn.bmp")); //drvo
	m_black_tex.LoadFromFile(_T("E:\\grafika\\Teksture\\blackhair.bmp"));//crna tex
	m_white_tex.LoadFromFile(_T("E:\\grafika\\Teksture\\Wall512.BMP"));//bela tex
	m_crep_tex.LoadFromFile(_T("E:\\grafika\\Teksture\\roof01.bmp"));
	m_grass_tex.LoadFromFile(_T("E:\\grafika\\Teksture\\grass1.bmp"));
	m_tex_camomreal.LoadFromFile(_T("E:\\grafika\\Teksture\\camomreal.bmp"));
	m_stone_wall_tex.LoadFromFile(_T("E:\\grafika\\Teksture\\zid.bmp"));
	m_dirt_tex.LoadFromFile(_T("E:\\grafika\\Teksture\\dirt2.bmp"));
	m_opengl_tex.LoadFromFile(_T("E:\\grafika\\Teksture\\OpenGL.bmp"));

	//glLineWidth(2.0);//
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//---------------------------------
	wglMakeCurrent(NULL, NULL);
}

void CGLRenderer::DrawScene(CDC *pDC)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	//---------------------------------
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	gluLookAt(eyeX, eyeY, eyeZ,
		centerX, centerY, centerZ,
		0.0, 1.0, 0.0);

	// Postavljanje svetla
	float light1_position[] = { 0.0, 35.0, 0.0, 1.0 };
	float spot_direction[] = { 0.0, -1.0, 0.0 };
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spot_direction);
	
	glEnable(GL_TEXTURE_2D);
	//DrawGrid(20.0, 10);
	//DrawAxes(20.0);
	
	//zidovi
	DrawWalls();

	//paleta
	glPushMatrix();
		m_tex3.Select();
		glTranslatef(0.0, -19.5, -5.0);
		DrawCuboid(15.0, 1.0, 15.0);
	glPopMatrix();
	
	glDisable(GL_TEXTURE_2D);
	//tabla
	glPushMatrix();
		glTranslatef(0.0, 0.0, -5.0);
		DrawBoard(15.0,9);
	glPopMatrix();

	//ker
	glEnable(GL_TEXTURE_2D);
		glPushMatrix();
			glTranslatef(-15.0, -15.5, -15.0);
			DrawKer();
		glPopMatrix();

		glPushMatrix();
			glTranslatef(-16.0, -18.0, -7.0);
			DrawHouse();
		glPopMatrix();
		
		glPushMatrix();
			m_tex2.Select();
			glTranslatef(-13.0, 0.0, -19.8);
			glRotatef(90.0, 1.0, 0.0, 0.0);
			DrawSide(12.0, 12.0, 4, 2, 4, 2);
		glPopMatrix();

		glPushMatrix();
			m_crep_tex.Select();
			glTranslatef(0.0, -17.5, -18.5);
			DrawBox(7.0, 5.0, 3.0, 5, 5, 5, 2, 2, 2);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(7.0, -16.25, -17.5);
			DrawBasket(5.0, 7.0, 0.5);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0.0, -19.0, -5.0);
			DrawLiftingMachine(1.0,5.0,5.0,0.0,0.0,0.0,0.0);
		glPopMatrix();

		glPushMatrix();
			m_tex_camomreal.Select();
			glTranslatef(0.0, 0.0, -3.5);
			RotateThorus(m_pomerajTorus);
		glPopMatrix();

		glPushMatrix();
			DrawLegoWall(2.0);
		glPopMatrix();

		glPushMatrix();
			//glScalef(0.5, 0.5, 0.5);
			glTranslatef(7.0, 3.0, -19.0);
			glRotatef(m_ugaoDijamanta, 0.0, 0.0, 1.0);
			DrawRingsFllower(5.0, 3.0, 1.3, 2.0);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0.0, -17.0, 10.0);
			DrawPartenon(0.5, 4.0);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(-7.5, 15.0, -7.5);
			glRotatef(180.0, 1.0, 0.0, 0.0);
			DrawConnectedCones(1.5, 5.0);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(-15.0, -12.5, 0.0);
			DrawDiamondOnTwine(1.0, 7.0, 1.5, 3.0, 1.5, 2.5, m_ugaoDijamanta);
		glPopMatrix();

		glPushMatrix();	
			glTranslatef(-15.0, 10.0, 10.0);
			m_opengl_tex.Select();
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
			DrawRubikCube(3.0, 3);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glPopMatrix();

		glPushMatrix();
			m_opengl_tex.Select();
			SetPrismMaterial(0.5, 0.0, 0.5);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
			//DrawPrism(5.0, 10.0, 5.0);
			DrawFigureShower(1.0, m_pomerajTorus/10.0, m_ugaoDijamanta, m_ugaoPrviZglob, m_ugaoDrugiZglob);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glPopMatrix();

	glDisable(GL_TEXTURE_2D);
	
	

	//robot
	/*glTranslatef(-5.0, 1.0, -5.0);
	glScalef(2.0, 2.0, 2.0);
	glColor3f(0.0, 1.0, 1.0);
	matTeapot.Select();
	DrawRobot(m_ugaoTrupa, m_ugaoGlave, m_ugaoLRamena, m_ugaoDRamena, m_ugaoLLakta, m_ugaoDLakta);
	*/


	//Vertex Array test
	/*glTranslatef(0.0, 5.0, 0.0);
	PrepareVACube(2.0);
	DrawVACube();*/

	
	//---------------------------------
	glFlush();
	SwapBuffers(pDC->m_hDC);
	wglMakeCurrent(NULL, NULL);
}

void CGLRenderer::Reshape(CDC *pDC, int w, int h)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	//---------------------------------
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(40, (double)w / (double)h, 1, 200);
	glMatrixMode(GL_MODELVIEW);
	//---------------------------------
	wglMakeCurrent(NULL, NULL);
}

void CGLRenderer::DestroyScene(CDC *pDC)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	// ...
	m_tex1.Relese();
	m_tex2.Relese();
	m_tex3.Relese();
	m_black_tex.Relese();
	m_white_tex.Relese();
	m_crep_tex.Relese();
	m_grass_tex.Relese();
	m_tex_camomreal.Relese();
	m_stone_wall_tex.Relese();
	m_dirt_tex.Relese();

	glDisable(GL_TEXTURE_2D);
	// ...
	wglMakeCurrent(NULL, NULL);
	if (m_hrc)
	{
		wglDeleteContext(m_hrc);
		m_hrc = NULL;
	}
}

void CGLRenderer::OnKeyDown(UINT nChar)
{
	/*
	if (nChar == 'Q')
		m_ugaoTrupa += 5.0;
	else if (nChar == 'W')
		m_ugaoTrupa -= 5.0;
	else if (nChar == 'E')
		m_ugaoGlave += 5.0;
	else if (nChar == 'R')
		m_ugaoGlave -= 5.0;
	else if (nChar == 'A')
		m_ugaoDRamena += 5.0;
	else if (nChar == 'Z')
		m_ugaoDRamena -= 5.0;
	else if (nChar == 'S')
		m_ugaoLRamena += 5.0;
	else if (nChar == 'X')
		m_ugaoLRamena -= 5.0;
	else if (nChar == 'D')
		m_ugaoDLakta += 5.0;
	else if (nChar == 'C')
		m_ugaoDLakta -= 5.0;
	else if (nChar == 'F')
		m_ugaoLLakta += 5.0;
	else if (nChar == 'V')
		m_ugaoLLakta -= 5.0;

	else*/ 
	if		(nChar == 'U')
		eyeX += 1.0;
	else if (nChar == 'I')
		eyeX -= 1.0;
	else if (nChar == 'J')
		eyeY += 1.0;
	else if (nChar == 'K')
		eyeY -= 1.0;
	else if (nChar == 'N')
		eyeZ += 1.0;
	else if (nChar == 'M') 
		eyeZ -= 1.0;

	else if (nChar == 'Z')
	{

		if (m_pravacRepa == true)
		{
			if ((m_ugaoRep + 5.0) < 60.0)
			{
				m_ugaoRep += 5.0;
			}
			else
			{
				m_pravacRepa = false;
				m_ugaoRep += 5.0;
			}
		}
		else
		{
			if ((m_ugaoRep - 5.0) > -60.0)
			{
				m_ugaoRep -= 5.0;
			}
			else
			{
				m_pravacRepa = true;
				m_ugaoRep -= 5.0;
			}
		}

	}

	else if (nChar == 'X')
	{
		m_ugaoCeleMasine += 5.0;
	}

	else if (nChar == 'C')
	{
		if (m_pravacPrviZglob == true)
		{
			if ((m_ugaoPrviZglob + 5.0) < 60.0)
				m_ugaoPrviZglob += 5.0;
			else
			{
				m_pravacPrviZglob = false;
				m_ugaoPrviZglob += 5.0;
			}
		}
		else
		{
			if ((m_ugaoPrviZglob - 5.0) > -30.0)
				m_ugaoPrviZglob -= 5.0;
			else
			{
				m_pravacPrviZglob = true;
				m_ugaoPrviZglob -= 5.0;
			}
		}
	}

	else if (nChar == 'V')
	{
		if (m_pravacDrugiZglob == true)
		{
			if ((m_ugaoDrugiZglob + 5.0) < 60.0)
				m_ugaoDrugiZglob += 5.0;
			else
			{
				m_pravacDrugiZglob = false;
				m_ugaoDrugiZglob += 5.0;
			}
		}
		else
		{
			if ((m_ugaoDrugiZglob - 5.0) > -30.0)
				m_ugaoDrugiZglob -= 5.0;
			else
			{
				m_pravacDrugiZglob = true;
				m_ugaoDrugiZglob -= 5.0;
			}
		}
	}

	else if (nChar == 'B')
	{
		m_ugaoDijamanta += 5.0;

		if (m_pravacTorus == true)
		{
			if ((m_pomerajTorus + 1.0) < 15.0)
				m_pomerajTorus += 1.0;
			else
			{
				m_pravacTorus = false;
				m_pomerajTorus += 1.0;
			}
		}
		else
		{
			if ((m_pomerajTorus - 1.0) > 0.0)
				m_pomerajTorus -= 1.0;
			else
			{
				m_pravacTorus = true;
				m_pomerajTorus -= 1.0;
			}
		}
	}

	else if (nChar == 'A')
	{
		m_ugaoRubikPrvaKolona += 5.0;
	}
	else if (nChar == 'S')
	{
		m_ugaoRubikDrugaKolona += 5.0;
	}
	else if (nChar == 'D')
	{
		m_ugaoRubikTrecaKolona += 5.0;
	}
}

void CGLRenderer::PrepareLighting()
{
	float light1_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
	float light1_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	float light1_specular[] = { 1.0, 1.0, 1.0, 1.0 };

	//Boja i intenzitet svetlosti
	glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular);

	//Slabljenje
	glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1.0);

	//Usmerenje izvoza
	glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 45.0);
	glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 1.0);

	//Aktiviranje
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHTING);

	//glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
}

void CGLRenderer::PrepareMaterials()
{
	matTeapot.SetAmbient(0.2, 0.1, 0.0, 0.0);
	matTeapot.SetDiffuse(0.7, 0.6, 0.0, 0.0);
	matTeapot.SetSpecular(1.0, 1.0, 1.0, 0.0);
	matTeapot.SetEmission(0.0, 0.0, 0.0, 0.0);
	matTeapot.SetShininess(16.0);

	matCube.SetAmbient(0.0, 0.0, 0.2, 0.0);
	matCube.SetDiffuse(0.1, 0.3, 0.7, 0.0);
	matCube.SetSpecular(1.0, 1.0, 1.0, 0.0);
	matCube.SetEmission(0.0, 0.0, 0.0, 0.0);
	matCube.SetShininess(32.0);

	matTable.SetAmbient(0.2, 0.0, 0.0, 0.0);
	matTable.SetDiffuse(0.5, 0.1, 0.0, 0.0);
	matTable.SetSpecular(0.8, 0.8, 0.8, 0.0);
	matTable.SetEmission(0.0, 0.0, 0.0, 0.0);
	matTable.SetShininess(64.0);

	//////
	matCrvena.SetAmbient(1.0, 0.0, 0.0, 0.0);
	matCrvena.SetDiffuse(1.0, 0.0, 0.0, 0.0);
	matCrvena.SetSpecular(0.0, 0.0, 0.0, 0.0);
	matCrvena.SetEmission(0.0, 0.0, 0.0, 0.0);
	matCrvena.SetShininess(16.0);

	matPlava.SetAmbient(0.0, 0.0, 1.0, 0.0);
	matPlava.SetDiffuse(0.0, 0.0, 1.0, 0.0);
	matPlava.SetSpecular(0.0, 0.0, 0.0, 0.0);
	matPlava.SetEmission(0.0, 0.0, 0.0, 0.0);
	matPlava.SetShininess(16.0);

	matNarndzasta.SetAmbient(1.0, 0.6, 0.0, 0.0);
	matNarndzasta.SetDiffuse(1.0, 0.6, 0.0, 0.0);
	matNarndzasta.SetSpecular(0.0, 0.0, 0.0, 0.0);
	matNarndzasta.SetEmission(0.0, 0.0, 0.0, 0.0);
	matNarndzasta.SetShininess(16.0);

	matZelena.SetAmbient(0.0, 1.0, 0.0, 0.0);
	matZelena.SetDiffuse(0.0, 1.0, 0.0, 0.0);
	matZelena.SetSpecular(0.0, 0.0, 0.0, 0.0);
	matZelena.SetEmission(0.0, 0.0, 0.0, 0.0);
	matZelena.SetShininess(16.0);

	matBela.SetAmbient(1.0, 1.0, 1.0, 0.0);
	matBela.SetDiffuse(1.0, 1.0, 1.0, 0.0);
	matBela.SetSpecular(0.0, 0.0, 0.0, 0.0);
	matBela.SetEmission(0.0, 0.0, 0.0, 0.0);
	matBela.SetShininess(16.0);

	matZuta.SetAmbient(1.0, 1.0, 0.0, 0.0);
	matZuta.SetDiffuse(1.0, 1.0, 0.0, 0.0);
	matZuta.SetSpecular(0.0, 0.0, 0.0, 0.0);
	matZuta.SetEmission(0.0, 0.0, 0.0, 0.0);
	matZuta.SetShininess(16.0);
}

//Dec-2011 - zad2b
void CGLRenderer::SetPrismMaterial(float r, float g, float b)
{
	float m_vAmbient[4] = { r / 2, g / 2, b / 2 };
	float m_vDifusse[4] = { r, g, b };
	float m_vSpecular[4] = { 1.0, 1.0, 1.0, 1.0 };
	float m_fShininess = 20.0;
	float m_vEmission[4] = { 0.0, 0.0, 0.0, 1.0 };

	glMaterialfv(GL_FRONT, GL_AMBIENT, m_vAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, m_vDifusse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m_vSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, m_fShininess);
	glMaterialfv(GL_FRONT, GL_EMISSION, m_vEmission);
}