#include "StdAfx.h"
#include "GLRenderer.h"
#include "GL\gl.h"
#include "GL\glu.h"
#include "GL\glut.h"
#define M_PI           3.14159265358979323846  /* pi */

void CGLRenderer::DrawFigureShower(float w, float dx, float alpha, float beta, float gama)
{
	glPushMatrix();
		DrawPrism(w, 15 * w, w);//a 

		glTranslatef(0.0, 7.5*w - dx, 0.0);
		glRotatef(alpha, 0.0, 1.0, 0.0);

		DrawPrism(1.5*w, 1.5*w, 1.5*w);//zglob a-b

		glRotatef(beta, 0.0, 0.0, 1.0);
		glTranslatef(1.5*w, 0.0, 0.0);

		DrawPrism(3 * w, w, w);//b

		glTranslatef(1.5*w, 0.0, 0.0);

		DrawPrism(1.5*w, 1.5*w, 1.5*w);//zglob b-c

		glTranslatef(1.5*w, 0.0, 0.0);
		glRotatef(gama, 1.0, 0.0, 0.0);

		DrawPrism(3 * w, w, w);//c

		glTranslatef(1.5*w, 0.0, 0.0);

		DrawPrism(1.5*w, 1.5*w, 1.5*w);//zglob c-d

		glTranslatef(0.0, 1.0*w, 0.0);
		glRotatef(gama, 0.0, 1.0, 0.0);

		DrawPrism(w, 4 * w, w);//d

		glTranslatef(0.0, 2.0*w, 0.0);
		
		DrawPrism(1.5*w, 1.5*w, 1.5*w);//zglob d-tus

		glTranslatef(0.75*w, 0.0, 0.0);

		DrawPrism(1.0*w, 2.5*w, 2.5*w);//tus

	glPopMatrix();
}

void CGLRenderer::DrawRubikCube(double a, int count)
{
	glPushMatrix();
	glTranslatef(-((double)count / 2 - 0.5) * a - ((int)count / 2) * a / 20, ((double)count / 2 - 0.5) * a + ((int)count / 2) * a / 20, -((double)count / 2 - 0.5) * a - ((int)count / 2) * a / 20);
	for (int i = 0; i < count; i++)
	{
		glPushMatrix();
		
		if (i == 0)
		{
			glTranslatef(0.0, -((double)count/2 - 0.5) * a - ((int)count/2) * a/20, ((double)count / 2 - 0.5) * a + ((int)count / 2) * a / 20);
			glRotatef(m_ugaoRubikPrvaKolona, 1.0, 0.0, 0.0);
			glTranslatef(0.0, ((double)count / 2 - 0.5) * a + ((int)count / 2) * a / 20, -((double)count / 2 - 0.5) * a - ((int)count / 2) * a / 20);
		}
		else if (i == 1)
		{
			glTranslatef(0.0, -((double)count / 2 - 0.5) * a - ((int)count / 2) * a / 20, ((double)count / 2 - 0.5) * a + ((int)count / 2) * a / 20);
			glRotatef(m_ugaoRubikDrugaKolona, 1.0, 0.0, 0.0);
			glTranslatef(0.0, ((double)count / 2 - 0.5) * a + ((int)count / 2) * a / 20, -((double)count / 2 - 0.5) * a - ((int)count / 2) * a / 20);
		}
		else if (i == 2)
		{
			glTranslatef(0.0, -((double)count / 2 - 0.5) * a - ((int)count / 2) * a / 20, ((double)count / 2 - 0.5) * a + ((int)count / 2) * a / 20);
			glRotatef(m_ugaoRubikTrecaKolona, 1.0, 0.0, 0.0);
			glTranslatef(0.0, ((double)count / 2 - 0.5) * a + ((int)count / 2) * a / 20, -((double)count / 2 - 0.5) * a - ((int)count / 2) * a / 20);
		}

		for (int j = 0; j < count; j++)
		{
			glPushMatrix();
			for (int k = 0; k < count; k++)
			{
				DrawRubikCubeSingle(a, i, j, k, count);
				glTranslatef(0.0, -(a + a / 20), 0.0);
			}
			glPopMatrix();
			glTranslatef(0.0, 0.0, a + a / 20);
		}
		glPopMatrix();
		glTranslatef(a + a / 20, 0.0, 0.0);
	}
	glPopMatrix();
}

//klk2-2010 - zad6
void CGLRenderer::DrawPartenon(double r, double h)
{
	m_white_tex.Select();
	//stubovi
	glPushMatrix();
		glTranslatef(0.0, -h / 2, 0.0);

		for (int i = 0; i < 8; i++)
		{
			DrawPillar(r, h);
			glTranslatef(5 * r, 0.0, 0.0);
		}

		glTranslatef(-5*r, 0.0, 5 * r);
		DrawPillar(r, h);

		glTranslatef(0.0, 0.0, 5 * r);
		DrawPillar(r, h);

		glTranslatef(0.0, 0.0, 5 * r);
		
		for (int i = 0; i < 8; i++)
		{
			DrawPillar(r, h);
			glTranslatef(-5 * r, 0.0, 0.0);
		}

		glTranslatef(5*r, 0.0, -5 * r);
		DrawPillar(r, h);

		glTranslatef(0.0, 0.0, -5 * r);
		DrawPillar(r, h);

	glPopMatrix();

	m_stone_wall_tex.Select();
	//postolje
	glPushMatrix();
		glTranslatef(17.5*r, -h / 2 - 0.6*r - 0.25*r, 7.5*r);
		DrawCuboid(40.0*r, 0.5*r, 20.0*r);

		glTranslatef(0.0, -0.5*r, 0.0);
		DrawCuboid(42.0*r, 1.0*r, 22.0*r);
	glPopMatrix();

	m_dirt_tex.Select();
	//krov
	glPushMatrix();
		glTranslatef(17.5*r, h / 2 + 0.5*r + 0.15*r, 7.5*r);
		DrawCuboid(38.0*r, 0.5*r, 18.0*r);
		//desna strana krova
		glPushMatrix();
			glTranslatef(0.0, tan(30.0 * M_PI / 180.0)*9*r+0.25*r, 0.0);
			glRotatef(30.0, 1.0, 0.0, 0.0);
			glTranslatef(0.0, 0.0, 3.0);
			DrawCuboid(40.0*r, 0.5*r, (9.0/cos(30.0*M_PI/180.0))*r+2*r);
		glPopMatrix();

		//leva strana krova
		glPushMatrix();
			glTranslatef(0.0, tan(30.0 * M_PI / 180.0) * 9 * r + 0.25*r, 0.0);
			glRotatef(-30.0, 1.0, 0.0, 0.0);
			glTranslatef(0.0, 0.0, -3.0);
			DrawCuboid(40.0*r, 0.5*r, (9.0 / cos(30.0*M_PI / 180.0))*r + 2 * r);
		glPopMatrix();
	glPopMatrix();
}

//klk2-2010 - zad5
//valjak dimenzija r,h
//na vrhu valjka kvadar stranice 2.5*r i debljine 0.5*r
//na dnu valjka dva kvadra debljine 0.3*r i dimenzija stranica 3*r i 4*r
void CGLRenderer::DrawPillar(double r, double h)
{
	glPushMatrix();
		DrawCylider(r, h);
		//donji kvadrati
		glPushMatrix();
			glTranslatef(0.0, -0.15*r, 0.0);
			DrawCuboid(3 * r, 0.3*r, 3 * r);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0.0, -0.45*r, 0.0);
			DrawCuboid(4 * r, 0.3*r, 4 * r);
		glPopMatrix();

		//gornji kvadrat
		glPushMatrix();
			glTranslatef(0.0, h + 0.25*r, 0.0);
			DrawCuboid(2.5*r, 0.5*r, 2.5*r);
		glPopMatrix();
	glPopMatrix();
}

void CGLRenderer::DrawRingsFllower(double a, double b, double h, double d)
{
	glPushMatrix();
		DrawRing(5.0, 3.0, 1.3, 2.0);
	glPopMatrix();

	glPushMatrix();
		m_crep_tex.Select();
		glRotatef(-60, 0.0, 0.0, 1.0);
		glTranslatef(0.0, 2 * (h + b), 0.0);
		DrawRing(5.0, 3.0, 1.3, 2.0);
	glPopMatrix();

	glPushMatrix();
		m_tex2.Select();
		glRotatef(-120, 0.0, 0.0, 1.0);
		glTranslatef(0.0, 2 * (h + b), 0.0);
		DrawRing(5.0, 3.0, 1.3, 2.0);
	glPopMatrix();

	glPushMatrix();
		m_tex3.Select();
		glRotatef(-180, 0.0, 0.0, 1.0);
		glTranslatef(0.0, 2 * (h + b), 0.0);
		DrawRing(5.0, 3.0, 1.3, 2.0);
	glPopMatrix();

	glPushMatrix();
		m_black_tex.Select();
		glRotatef(-240, 0.0, 0.0, 1.0);
		glTranslatef(0.0, 2 * (h + b), 0.0);
		DrawRing(5.0, 3.0, 1.3, 2.0);
	glPopMatrix();

	glPushMatrix();
		m_white_tex.Select();
		glRotatef(-300, 0.0, 0.0, 1.0);
		glTranslatef(0.0, 2 * (h + b), 0.0);
		DrawRing(5.0, 3.0, 1.3, 2.0);
	glPopMatrix();

	glPushMatrix();
		m_grass_tex.Select();
		glRotatef(-360, 0.0, 0.0, 1.0);
		glTranslatef(0.0, 2 * (h + b), 0.0);
		DrawRing(5.0, 3.0, 1.3, 2.0);
	glPopMatrix();
}

//Nov-2011 - zad2b
void CGLRenderer::DrawRing(double a, double b, double h, double d)
{
	

	glPushMatrix();
		glTranslatef(0.0, -b - h, 0.0);
		DrawTrapezoid(a, b, h, d);
	glPopMatrix();

	glRotatef(-60.0, 0.0, 0.0, 1.0);
	glPushMatrix();
		glTranslatef(0.0, -b - h, 0.0);
		DrawTrapezoid(a, b, h, d);
	glPopMatrix();

	glRotatef(-60.0, 0.0, 0.0, 1.0);
	glPushMatrix();
		glTranslatef(0.0, -b - h, 0.0);
		DrawTrapezoid(a, b, h, d);
	glPopMatrix();

	glRotatef(-60.0, 0.0, 0.0, 1.0);
	glPushMatrix();
		glTranslatef(0.0, -b - h, 0.0);
		DrawTrapezoid(a, b, h, d);
	glPopMatrix();

	glRotatef(-60.0, 0.0, 0.0, 1.0);
	glPushMatrix();
		glTranslatef(0.0, -b - h, 0.0);
		DrawTrapezoid(a, b, h, d);
	glPopMatrix();

	glRotatef(-60.0, 0.0, 0.0, 1.0);
	glPushMatrix();
		glTranslatef(0.0, -b - h, 0.0);
		DrawTrapezoid(a, b, h, d);
	glPopMatrix();
}

//Oct-2014 - zad2d
void CGLRenderer::DrawLegoWall(double wallWidth)
{
	glTranslatef(19.0, -19.0, -10.0);
	glRotatef(-90.0, 0.0, 1.0, 0.0);

	//prvi red
	DrawLegoBox(20.0, 2.0, 2.0);

	//drugi red
	glTranslatef(0.0, 2.0, 0.0);
	glPushMatrix();
		m_tex1.Select();
		glTranslatef(-5.5, 0.0, 0.0);
		DrawLegoBox(9.0, 2.0, 2.0);

		m_tex2.Select();
		glTranslatef(9.0, 0.0, 0.0);
		DrawLegoBox(9.0, 2.0, 2.0);
	glPopMatrix();

	//treci red
	glTranslatef(0.0, 2.0, 0.0);
	glPushMatrix();
		m_tex3.Select();
		glTranslatef(-7.75, 0.0, 0.0);
		DrawLegoBox(4.5, 2.0, 2.0);

		m_black_tex.Select();
		glTranslatef(4.5, 0.0, 0.0);
		DrawLegoBox(4.5, 2.0, 2.0);

		m_white_tex.Select();
		glTranslatef(4.5, 0.0, 0.0);
		DrawLegoBox(4.5, 2.0, 2.0);
	glPopMatrix();

	//cetvrti red
	glTranslatef(0.0, 2.0, 0.0);
	glPushMatrix();
		m_crep_tex.Select();
		glTranslatef(-7.5, 0.0, 0.0);
		DrawLegoBox(5, 2.0, 2.0);

		m_grass_tex.Select();
		glTranslatef(5.0, 0.0, 0.0);
		DrawLegoBox(5, 2.0, 2.0);
	glPopMatrix();

	//peti red
	glTranslatef(0.0, 2.0, 0.0);
	glPushMatrix();
		m_tex_camomreal.Select();
		glTranslatef(-6.25, 0.0, 0.0);
		DrawLegoBox(7.5, 2.0, 2.0);
	glPopMatrix();
}

//Oct-2014 - zad2c
void CGLRenderer::DrawLegoBox(double x, double y, double z)
{
	double cylinderHeight = y / 100.0 * 30;
	double cylinderRadius = z / 100.0 * 15;
	double cylinderOffsetOnX = x / 3;
	double cylinderOffsetOnZ = z / 5;

	glPushMatrix();
		DrawBox(x, y, z, 2, 2, 2, 1, 1, 1);

		matTeapot.Select();
		glTranslatef(-(x / 2)+(cylinderOffsetOnX / 2), y / 2, -(z / 2) + (cylinderOffsetOnZ / 2) + cylinderRadius);
		//prvi red
		glPushMatrix();
		for (int i = 0; i < 3; i++)
		{
			DrawCylider(cylinderRadius, cylinderHeight);
			glTranslatef(cylinderOffsetOnX, 0.0, 0.0);
		}
		glPopMatrix();
		
		//drugi red
		glTranslatef(0.0, 0.0, cylinderOffsetOnZ*2 + cylinderRadius);
		glPushMatrix();
		for (int i = 0; i < 3; i++)
		{
			DrawCylider(cylinderRadius, cylinderHeight);
			glTranslatef(cylinderOffsetOnX, 0.0, 0.0);
		}
		glPopMatrix();
	glPopMatrix();
}

//Apr-2013 - zad3
//r - poluprecnik osnove
//h - visina jedne kupe
void CGLRenderer::DrawConnectedCones(double r, double h)
{
	matCube.Select();

	m_dirt_tex.Select();
	//prvi par kupa - dole
	DrawCone(r, h); //gornja kupa
	glPushMatrix();
		glRotatef(180.0, 1.0, 0.0, 0.0); 
		DrawCone(r, h);
	glPopMatrix();

	glRotatef(m_ugaoDijamanta, 0.0, 1.0, 0.0);
	glPushMatrix();

	m_stone_wall_tex.Select();
	//drugi par kupa
	glRotatef(-120.0, 0.0, 1.0, 0.0);
	glPushMatrix();
	glTranslatef(-h, h, 0.0);
	glRotatef(90.0, 0.0, 0.0, 1.0);
	
	DrawCone(r, h); //gornja kupa
	glPushMatrix();
		glRotatef(180.0, 1.0, 0.0, 0.0); 
		DrawCone(r, h);
	glPopMatrix();
	glPopMatrix();
	
	m_tex_camomreal.Select();
	//treci par kupa
	glRotatef(-120.0, 0.0, 1.0, 0.0);
	glPushMatrix();
	glTranslatef(-h, h, 0.0);
	glRotatef(90.0, 0.0, 0.0, 1.0);

	DrawCone(r, h); //gornja kupa
	glPushMatrix();
		glRotatef(180.0, 1.0, 0.0, 0.0); 
		DrawCone(r, h);
	glPopMatrix();
	glPopMatrix();

	m_tex3.Select();
	//cetvrti par kupa
	glRotatef(-120.0, 0.0, 1.0, 0.0);
	glPushMatrix();
	glTranslatef(-h, h, 0.0);
	glRotatef(90.0, 0.0, 0.0, 1.0);

	DrawCone(r, h); //gornja kupa
	glPushMatrix();
	glRotatef(180.0, 1.0, 0.0, 0.0); 
	DrawCone(r, h);
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
}

//Sep-2017 - zad2d
//visina stubova je hS kao i duzina grede
//srtanica osnove stuba je aS
//visina osnove tela na vrhu kanapa je aS, a stranica je aR
//priamide u dijamantu imaju stranicu side i visinu height
//duzina kanapa je 2*height
//centar tela na vrhu kanapa je na offset od levog stuga dakle na toliko je i dijamant i kanap
void CGLRenderer::DrawDiamondOnTwine(double aS, double hS, double aR, double size, double height, double offset, double angle)
{
	glPushMatrix();
		m_dirt_tex.Select();
		//greda
		glPushMatrix();
			glRotatef(90.0, 0.0, 0.0, 1.0);
			DrawColumn(aS, hS);
		glPopMatrix();

		//levi stub
		glPushMatrix();
			glTranslatef(-(hS / 2) + (aS / 2), -(hS / 2) - (aS / 2), 0.0);
			DrawColumn(aS, hS);
		glPopMatrix();
		
		//desni stub
		glPushMatrix();
			glTranslatef((hS / 2) - (aS / 2), -(hS / 2) - (aS / 2), 0.0);
			DrawColumn(aS, hS);
		glPopMatrix();

		m_white_tex.Select();

		offset = -(hS / 2) + (aS / 2) + offset;

		if (offset > (-(hS / 2) + (aS / 2)) && offset < ((hS / 2) - (aS / 2)))
		{
			glTranslatef(offset, -aS, 0.0);
			glRotatef(angle, 0.0, 1.0, 0.0);

			//telo na vrhu
			DrawCuboid(aS, aS, aR);

			//kanap
			glBegin(GL_LINES);
				glTranslatef(0.0, -(aS / 2), 0.0);
				glVertex3f(0.0, 0.0, 0.0);
				glVertex3f(0.0, -2 * height, 0.0);
			glEnd();

			//dijamant
			glTranslatef(0.0, -(3 * height) , 0.0);
			DrawDiamond(size, height, 5);
		}

	glPopMatrix();
}

//Jan-2018 - zad2c
//valjak poluprecnik r i debljine 2*w
//vertikalni stub duzine d
//onda zglob (1.5w*1.5w*1.5w) i drugi stub(w*w*d) iste duzine pa opet zglob(1.5w*1.5w*1.5w) i treci stub iste duzine
//na kraju je korpa oblika kvadra dimenzija 6*w x 0.5*h x 6*w
void CGLRenderer::DrawLiftingMachine(double w, double h, double d, double dx, double alpha, double beta, double gamma)
{
	glPushMatrix();
		//postolje - valjak
		glRotatef(m_ugaoCeleMasine, 0.0, 1.0, 0.0);
		DrawCylider(3.5, 2 * w);

		//vertikalni stub
		glTranslatef(0.0, w + d / 2, 0.0);
		DrawBox(w, d, w, 1, 1, 1, 1, 1, 1);

		//prvi zglob
		glTranslatef(0.0, d / 2 + 1.5*w / 2, 0.0);
		DrawBox(1.5*w, 1.5*w, 1.5*w, 1, 1, 1, 1, 1, 1);
		//drugi stub
		glRotatef(m_ugaoPrviZglob, 0.0, 0.0, 1.0);
		glTranslatef(d / 2, 0.0, 0.0);
		DrawBox(d, w, w, 1, 1, 1, 1, 1, 1);

		//drugi zglob
		glTranslatef(d / 2 + 1.5*w / 2, 0.0, 0.0);
		DrawBox(1.5*w, 1.5*w, 1.5*w, 1, 1, 1, 1, 1, 1);
					
		//treci stub
		glRotatef(m_ugaoDrugiZglob, 0.0, 0.0, 1.0);
		glTranslatef(d / 2, 0.0, 0.0);
		DrawBox(d, w, w, 1, 1, 1, 1, 1, 1);
					
		//korpa
		glTranslatef(d / 2 + 3 * w, 0.0, 0.0);
		glRotatef(-(m_ugaoPrviZglob + m_ugaoDrugiZglob), 0.0, 0.0, 1.0);
		DrawBasket(6 * w, 0.5*h, w);
	glPopMatrix();
}

void CGLRenderer::DrawHouse()
{
	double a = 7.0;
	double b = 4.0;
	double c = 4.0;
	double roofAngle = 90.0;

	glPushMatrix();
	m_white_tex.Select();
	DrawCuboid(a, b, c);
	glTranslatef(0.0, c/2, 0.0);
	DrawTriangularPrism(a, b, roofAngle);
	glPopMatrix();
}

void CGLRenderer::DrawKer()
{
	glPushMatrix();

	//trup
	glPushMatrix();
	m_black_tex.Select();
	DrawCuboid(3.0, 3.0, 5.0);
	glPopMatrix();
	//glava
	glPushMatrix();
	glRotatef(m_ugaoRep, 0.0, 0.0, 1.0);
	glTranslatef(0.0, 0.0, -2.5);
	glRotatef(45.0, 1.0, 0.0, 0.0);
	glTranslatef(0.0, 0.0, -1.5);
	m_white_tex.Select();
	glutSolidSphere(1.5, 20, 20);
	glPopMatrix();

	//rep
	glPushMatrix();
	glRotatef(m_ugaoRep, 0.0, 0.0, 1.0);
	glTranslatef(0.0, 1.0, 2.5);
	glRotatef(-45.0, 1.0, 0.0, 0.0);
	glTranslatef(0.0, 0.0, 1.5);
	m_tex2.Select();
	DrawCuboid(0.5, 0.5, 3.0);
	glPopMatrix();

	//prednja leva noga
	glPushMatrix();
	glTranslatef(-1.25, -1.5, -2.25);
	glRotatef(m_ugaoRep, 1.0, 0.0, 0.0);
	glTranslatef(0.0, -1.5, 0.0);
	m_white_tex.Select();
	DrawCuboid(0.5, 3.0, 0.5);
	glPopMatrix();

	//prednja desna noga
	glPushMatrix();
	glTranslatef(1.25, -1.5, -2.25);
	glRotatef(m_ugaoRep, 1.0, 0.0, 0.0);
	glTranslatef(0.0, -1.5, 0.0);
	m_white_tex.Select();
	DrawCuboid(0.5, 3.0, 0.5);
	glPopMatrix();

	//zadnja leva noga
	glPushMatrix();
	glTranslatef(-1.25, -1.5, 2.25);
	glRotatef(-m_ugaoRep, 1.0, 0.0, 0.0);
	glTranslatef(0.0, -1.5, 0.0);
	m_white_tex.Select();
	DrawCuboid(0.5, 3.0, 0.5);
	glPopMatrix();

	//zadnja desna noga
	glPushMatrix();
	glTranslatef(1.25, -1.5, 2.25);
	glRotatef(-m_ugaoRep, 1.0, 0.0, 0.0);
	glTranslatef(0.0, -1.5, 0.0);
	m_white_tex.Select();
	DrawCuboid(0.5, 3.0, 0.5);
	glPopMatrix();

	glPopMatrix();
}

void CGLRenderer::DrawRobot(double ugaoTrupa, double ugaoGlave, double ugaoLRamena, double ugaoDRamena, double ugaoLLakta, double ugaoDLakta)
{
	glPushMatrix();
		glRotatef(ugaoTrupa, 0.0, 1.0, 0.0); //rotacija trupa oko Y ose
		//cratanje trupa
		glPushMatrix();
			glScalef(1.0, 2.0, 0.5);
			DrawCube(1.0);
		glPopMatrix();

		//cratanje desne nadlaktice
		glPushMatrix();
			glTranslatef(0.65, 1.0, 0.0);
			glRotatef(-ugaoDRamena, 1.0, 0.0, 0.0);
			glTranslatef(0.0, -0.5, 0.0);
		
			glPushMatrix();
				glScalef(0.3, 1.0, 0.5);
				DrawCube(1.0);
			glPopMatrix();

			//crtanje desne podlaktice
			glTranslatef(0.0, -0.5, 0.0);
			glRotatef(-ugaoDLakta, 1.0, 0.0, 0.0);
			glTranslatef(0.0, -0.5, 0.0);
			
			glPushMatrix();
				glScalef(0.3, 1.0, 0.5);
				DrawCube(1.0);
			glPopMatrix();
		glPopMatrix();

	//cratanje levee nadlaktice
	glPushMatrix();
	glTranslatef(-0.65, 1.0, 0.0);
	glRotatef(-ugaoLRamena, 1.0, 0.0, 0.0);
	glTranslatef(0.0, -0.5, 0.0);
	glPushMatrix();
	glScalef(0.3, 1.0, 0.5);
	DrawCube(1.0);
	glPopMatrix();

	//crtanje leve podlaktice
	glTranslatef(0.0, -0.5, 0.0);
	glRotatef(-ugaoLLakta, 1.0, 0.0, 0.0);
	glTranslatef(0.0, -0.5, 0.0);
	glPushMatrix();
	glScalef(0.3, 1.0, 0.5);
	DrawCube(1.0);
	glPopMatrix();
	glPopMatrix();

	//crtanje glave
	glPushMatrix();
	glTranslatef(0.0, 1.3, 0.0);
	glRotatef(ugaoGlave, 0.0, 1.0, 0.0);
	glScalef(0.6, 0.6, 0.6);
	DrawCube(1.0);
	glPopMatrix();

	glPopMatrix();
}