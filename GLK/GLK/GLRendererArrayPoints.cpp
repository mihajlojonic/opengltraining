#include "StdAfx.h"
#include "GLRenderer.h"
#include "GL\gl.h"
#include "GL\glu.h"


void CGLRenderer::PrepareVACube(float a)
{
	vert[0] = -a / 2; vert[1] = -a / 2; vert[2] = a / 2; // vert0
	vert[3] = a / 2; vert[4] = -a / 2; vert[5] = a / 2; // vert1
	vert[6] = a / 2; vert[7] = a / 2; vert[8] = a / 2; // vert2
	vert[9] = -a / 2; vert[10] = a / 2; vert[11] = a / 2; // vert3
	vert[12] = -a / 2; vert[13] = -a / 2; vert[14] = -a / 2; // vert4
	vert[15] = a / 2; vert[16] = -a / 2; vert[17] = -a / 2; // vert5
	vert[18] = a / 2; vert[19] = a / 2; vert[20] = -a / 2; // vert6
	vert[21] = -a / 2; vert[22] = a / 2; vert[23] = -a / 2; // vert7

	col[0] = 0.0; col[1] = 0.0; col[2] = 0.0; // col0
	col[3] = 1.0; col[4] = 0.0; col[5] = 0.0; // col1
	col[6] = 1.0; col[7] = 1.0; col[8] = 0.0; // col2
	col[9] = 1.0; col[10] = 1.0; col[11] = 1.0; // col3
	col[12] = 0.0; col[13] = 1.0; col[14] = 0.0; // col4
	col[15] = 0.0; col[16] = 1.0; col[17] = 1.0; // col5
	col[18] = 0.0; col[19] = 0.0; col[20] = 1.0; // col6
	col[21] = 0.0; col[22] = 0.0; col[23] = 0.0; // col7
}

void CGLRenderer::DrawVACube()
{
	glVertexPointer(3, GL_FLOAT, 0, vert);
	glColorPointer(3, GL_FLOAT, 0, col);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glBegin(GL_QUADS);

	//prednja strana
	glArrayElement(0);
	glArrayElement(1);
	glArrayElement(2);
	glArrayElement(3);

	//desna strana
	glArrayElement(1);
	glArrayElement(5);
	glArrayElement(6);
	glArrayElement(2);

	//zadnja strana
	glArrayElement(7);
	glArrayElement(6);
	glArrayElement(5);
	glArrayElement(4);

	//leva strana
	glArrayElement(0);
	glArrayElement(3);
	glArrayElement(7);
	glArrayElement(4);

	//gornja strana
	glArrayElement(7);
	glArrayElement(3);
	glArrayElement(2);
	glArrayElement(6);

	//donja strana
	glArrayElement(0);
	glArrayElement(4);
	glArrayElement(5);
	glArrayElement(1);

	glEnd();
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
}